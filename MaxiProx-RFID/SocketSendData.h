/* 
 * File:   SocketSendData.h
 * Author: Marco
 *
 * Created on 22 March 2014, 21:31
 */
#include <stdint.h>
#include <string>

using namespace std;

#ifndef SOCKETSENDDATA_H
#define	SOCKETSENDDATA_H

class SocketSendData {
public:
    SocketSendData();

    virtual ~SocketSendData();

    uint8_t * bytesToSend;
	string stringToSend;
};

#endif	/* SOCKETSENDDATA_H */

