
/*
 * File:   SocketServerOutThread.cpp
 * Author: marco
 *
 * Created on febr, 2014, 13:36
 */
#include "global.h"

#include "SocketServerOutThread.h"
#include "SocketSendData.h"

 // We don't have a debug socket...
 //#include "SocketDebugThread.h"

 //#include "UartProcessor.h"

 // threading
#include <pthread.h>

// for standard io ie count cin
#include <iostream>

#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/ioctl.h>

// queue's 


#include <vector>

using namespace std;

vector<SocketSendData*> queueToSend;
vector<SocketSendData*> queueSending;
vector<SocketSendData*> queueReceived;

pthread_t socketServerOutThread;
pthread_attr_t attrsocketServerOutThread;
pthread_mutex_t mutexQueueSocketServerOutThread;

bool gt_stopSocketServerOutThread = false;
bool gt_SocketServerOutThreadStopped = false;
bool socketClientConnected = false;

int sockserveroutfd, newsockserveroutfd, serveroutporto;
socklen_t serveroutclilen;
uint8_t socketServerOutBuffer[256];
struct sockaddr_in serveroutserv_addr, serveroutcli_addr;

void startSocketServerOutThread() {

	// determine stacksize for thread
	std::size_t stacksize;
	pthread_attr_init(&attrsocketServerOutThread);
	pthread_attr_getstacksize(&attrsocketServerOutThread, &stacksize);
	//cout << "Default stack size = " << stacksize << "\n";

	//init mutex for Queue
	pthread_mutex_init(&mutexQueueSocketServerOutThread, nullptr);

	pthread_create(&socketServerOutThread, nullptr, runSocketServerOutThread, nullptr);
	msg("SocketServerOutThread Started");
}

void stopSocketServerOutThread() {
	pthread_mutex_lock(&mutexQueueSocketServerOutThread);
	gt_stopSocketServerOutThread = true;
	pthread_mutex_unlock(&mutexQueueSocketServerOutThread);

	dmsg("Joining SocketServerOutThread");

	pthread_join(socketServerOutThread, nullptr);
	dmsg("Joined SocketServerOutThread");

	pthread_mutex_destroy(&mutexQueueSocketServerOutThread);
	gt_SocketServerOutThreadStopped = true;
}

void errorServerOut(const char *msg) {
	perror(msg);
	//exit(1);
}

void * runSocketServerOutThread(void *ptr) {
	sockserveroutfd = socket(AF_INET, SOCK_STREAM, 0);

	// reuse socket if needed
	int optval = 1;
	setsockopt(sockserveroutfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

	if (sockserveroutfd < 0)
		errorServerOut("ERROR opening serveroutsocket");

	bzero((char *)&serveroutserv_addr, sizeof(serveroutserv_addr));
	serveroutporto = 5884;
	serveroutserv_addr.sin_family = AF_INET;
	serveroutserv_addr.sin_addr.s_addr = INADDR_ANY;
	serveroutserv_addr.sin_port = htons(short(serveroutporto));

	if (bind(sockserveroutfd, reinterpret_cast<struct sockaddr *>(&serveroutserv_addr), sizeof(serveroutserv_addr)) < 0)
		errorServerOut("ERROR on binding");

	tickSocketServerOutThread();

	close(sockserveroutfd);

	msg("SocketServerOutThread Stopped");
	pthread_exit(static_cast<void *>(nullptr));
	return nullptr;
}

void addToQueueToSend(SocketSendData * usd) {

	pthread_mutex_lock(&mutexQueueSocketServerOutThread);
	if (queueToSend.size() > 1) {
		errorServerOut("ERROR queueToSend greater than one");
		socketClientConnected = false;
	}
	queueToSend.push_back(usd);
	SocketSendData * usdff = new SocketSendData();
	usdff->stringToSend = "sendData ToATX: " + zeroPadding(int(usd->bytesToSend[0]), 3) + " " + zeroPadding(int(usd->bytesToSend[1]), 3) + " " + zeroPadding(int(usd->bytesToSend[2]), 3);

	// We don't have a debug server socket...
	//addToQueueToSendDebug(usdff);

	pthread_mutex_unlock(&mutexQueueSocketServerOutThread);
}

//void addToUartThreadOutResponse(SocketSendData * usd) {
//    pthread_mutex_lock(&mutexQueueSocketServerOutThread);
//    queueReceived.push_back(usd);
//    pthread_mutex_unlock(&mutexQueueSocketServerOutThread);
//
//}

void tickSocketServerOutThread() {
	int n, nRead;

	while (true) {

		listen(sockserveroutfd, 5);
		serveroutclilen = sizeof(serveroutcli_addr);

		newsockserveroutfd = accept(sockserveroutfd, reinterpret_cast<struct sockaddr *>(&serveroutcli_addr), &serveroutclilen);
		if (newsockserveroutfd < 0) {
			errorServerOut("ERROR on accept");
			socketClientConnected = false;
		}
		else {
			cout << "client connected!" << endl;

			struct timeval tv;
			tv.tv_sec = 10;  /* 30 Secs Timeout */
			setsockopt(newsockserveroutfd, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char*>(&tv), sizeof(struct timeval));
			socketClientConnected = true;
			//char* welcomeMessage = "Hello World!\r\n";
			//int n = write(newsockserveroutfd, static_cast<const void*>(welcomeMessage), 14);
			//if (n < 0) errorServerOut("ERROR writing to socket");
		}

		while (socketClientConnected) {

			pthread_mutex_lock(&mutexQueueSocketServerOutThread);

			for (uint i = 0; i < queueToSend.size(); i++) {
				queueSending.push_back(queueToSend[i]);
			}
			queueToSend.clear();
			pthread_mutex_unlock(&mutexQueueSocketServerOutThread);

			SocketSendData * usd;
			const char *p_str;
			for (uint x = 0; x < queueSending.size(); x++) {

				usd = queueSending[x];

				p_str = usd->stringToSend.c_str();

				n = int(write(newsockserveroutfd, p_str, usd->stringToSend.length()));

				
				/*usd = queueSending[x];

				n = write(newsockserveroutfd, static_cast<const void*>(usd->bytesToSend), 3);*/


				if (n < 0)
				{
					errorServerOut("ERROR writing to socket");
				}

				//write(fdoutput, (const void*) usd->bytesToSend, 3);
				//write(fdoutput, (const void*) &usd->bytesToSend[1] ,1);
				//write(fdoutput, (const void*) &usd->bytesToSend[2] ,1);
				//write(fdoutput,  "\n\r" ,2);

				// temp disable reading from socket
				/*bool received = false;

				while (!received) {
					pthread_mutex_lock(&mutexQueueSocketServerOutThread);

					bzero(socketServerOutBuffer, 256);
					n = read(newsockserveroutfd, socketServerOutBuffer, 255);

					if (n > 0) {
						if (usd->bytesToSend[0] == socketServerOutBuffer[0] && usd->bytesToSend[1] == socketServerOutBuffer[1] && usd->bytesToSend[2] == socketServerOutBuffer[2]) {
							received = true;
						}
					}
					pthread_mutex_unlock(&mutexQueueSocketServerOutThread);
					usleep(500);

				}*/

				delete queueSending[x];

			}

			//int bytesAvailable;
			//ioctl(newsockserveroutfd, FIONREAD, &bytesAvailable);
			//if (bytesAvailable > 0)
			//{
			//	pthread_mutex_lock(&mutexQueueSocketServerOutThread);
			//	bzero(socketServerOutBuffer, 256);

			//	// check if client sends something and is still connected
			//	nRead = recv(newsockserveroutfd, socketServerOutBuffer, 1, MSG_PEEK);

			//	if (nRead > 0) {
			//		//if (usd->bytesToSend[0] == socketServerOutBuffer[0] && usd->bytesToSend[1] == socketServerOutBuffer[1] && usd->bytesToSend[2] == socketServerOutBuffer[2]) {
			//		//	//received = true;
			//		//}
			//		cout << "n > 0, bytesAvailable = " << bytesAvailable << "\r\n";
			//	}
			//	else {
			//		if (nRead < 0) errorServerOut("ERROR reading from socket, n < 0");
			//		else if (nRead == 0)
			//		{
			//			errorServerOut("ERROR socket closed??, n == 0");
			//			socketClientConnected = false;
			//		}
			//	}
			//	pthread_mutex_unlock(&mutexQueueSocketServerOutThread);
			//}
			//else if (bytesAvailable == -1)
			//{
			//	errorServerOut("ERROR bytesAvailable return -1");
			//	socketClientConnected = false;
			//}


			usleep(10000);

			// Clears the queue
			queueSending.clear();

		}

		close(newsockserveroutfd);
		msg("client disconnected!");

		/*
		while (true) {
			// cout << "SocketDebug while true" << endl;
			pthread_mutex_lock(&mutexQueueSocketServerOutThread);

			for (uint i = 0; i < queueToSend.size(); i++) {
				queueSending.push_back(queueToSend[i]);
			}
			queueToSend.clear();
			pthread_mutex_unlock(&mutexQueueSocketServerOutThread);

			SocketSendData * usd;
			const char *p_str;
			for (uint x = 0; x < queueSending.size(); x++) {
				usd = queueSending[x];

				p_str = usd->stringToSend.c_str();
				n = int(write(newsockserveroutfd, p_str, usd->stringToSend.length()));


				//n = write(newsockfddebug, (const void*) usd->bytesToSend, 3);

				if (n < 0) errorServerOut("ERROR writing to socket");



				//write(fdoutput, (const void*) usd->bytesToSend, 3);
				//write(fdoutput, (const void*) &usd->bytesToSend[1] ,1);
				//write(fdoutput, (const void*) &usd->bytesToSend[2] ,1);
				//write(fdoutput,  "\n\r" ,2);
				bool received = false;

				while (!received) {
					pthread_mutex_lock(&mutexQueueSocketServerOutThread);

					bzero(socketServerOutBuffer, 256);
					n = int(read(newsockserveroutfd, socketServerOutBuffer, 255));
					if (n == 0) break;

					//if (n > 0) {
					//if (usd->bytesToSend[0] == socketBufferDebug[0] && usd->bytesToSend[1] == socketBufferDebug[1] && usd->bytesToSend[2] == socketBufferDebug[2]) {
					received = true;
					//}
					//}
					pthread_mutex_unlock(&mutexQueueSocketServerOutThread);
					usleep(500);

				}

				delete queueSending[x];

			}
			usleep(10000);

			// Clears the queue
			queueSending.clear();
			if (n == 0) break;
		}
		*/

		//close(newsockserveroutfd);
		//cout << "ServerOut socket closed!" << endl;
		//return gt_stopSocketServerOutThread;
	}
}

