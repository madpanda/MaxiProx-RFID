/* 
 * File:   SocketServerOutThread.h
 * Author: Marco
 *
 * Created on 7 maart 2014, 11:39
 */

#ifndef SOCKETSERVEROUTTHREAD_H
#define	SOCKETSERVEROUTTHREAD_H

#include "SocketSendData.h"
// threading

void startSocketServerOutThread();
void stopSocketServerOutThread();


void *runSocketServerOutThread(void *ptr) ;
void tickSocketServerOutThread();

void addToQueueToSend(SocketSendData * usd);

// void addToUartThreadOutResponse(SocketSendData * usd);

extern bool gt_SocketServerOutThreadStopped;

#endif	/* SOCKETSERVEROUTTHREAD_H */

