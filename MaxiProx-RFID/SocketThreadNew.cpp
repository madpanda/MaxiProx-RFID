/*
* File:   SocketThread.cpp
* Author: marco
*
* Created on May 12, 2012, 12:31 AM
*/



#include "SocketThreadNew.h"
#include <vector>
#include "OrganDataLoader.h"

using namespace std;

// threading
#include <pthread.h>
// for standard io ie count cin
#include <iostream>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>


pthread_t socketThreadNew;
pthread_attr_t attrsocketThreadNew;
pthread_mutex_t mutexQueueSocketThreadNew;

bool gt_stopSocketThreadNew = false;
bool gt_SocketThreadNewStopped = false;

int socknewfd, newsocknewfd, portnonew;
socklen_t clilennew;
char socketBuffernew[256];
struct sockaddr_in serv_addrnew, cli_addrnew;

string DeTune(Properties message)
{
	{
		int manNr = CInt(message["manualNr"]);
		int stopNr = CInt(message["stopNr"]);
		int rankNr = CInt(message["rankNr"]);
		double percLow = CDbl(message["lowPerc"]);
		double percHigh = CDbl(message["highPerc"]);
		organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->tuneRank(percLow, percHigh);
	}


}

string GetEqValues(Properties message)
{

	/*
	* param 1: mannr
	* param 2: stopnr
	* param 3: ranknr
	* param 4: notenr
	* param 5: qtynotes
	* param 6: harmonics equalizer (0 or 1)
	*/

	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = CInt(message["noteNr"]);
	int qtyNotes = CInt(message["qtyNotes"]);
	int harmonicsEqualizer = CInt(message["harmonicsEqualizer"]);

	string sendString = "";
	string sendStringToAdd = "";

	for (int bandNr = 0; bandNr < 31; bandNr++)
	{
		if (harmonicsEqualizer == 1)
		{
			sendStringToAdd = CStr(organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(noteNr)->pipeData.eqharmValue[bandNr]);
		}
		else
		{
			sendStringToAdd = CStr(organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(noteNr)->pipeData.eqoctValue[bandNr]);
		}
		if (sendStringToAdd.length() == 1) {
			sendStringToAdd = "0" + sendStringToAdd;
		}
		sendString += sendStringToAdd;

	}
	return "eqValues=" + sendString;

}

string GetEqValuesForChannel(Properties message)
{
	/*
	* param 1: mannr
	* param 2: stopnr
	* param 3: ranknr
	* param 4: notenr
	* param 5: qtynotes
	* param 6: harmonics equalizer (0 or 1)
	* param 7: channel
	*/

	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = CInt(message["noteNr"]);
	int qtyNotes = CInt(message["qtyNotes"]);
	int harmonicsEqualizer = CInt(message["harmonicsEqualizer"]);
	int channel = CInt(message["channel"]);


	string sendString = "";
	string sendStringToAdd = "";

	for (int manNr = 0; manNr < organ.organData.nrOfManuals; manNr++)
	{
		for (int stopNr = 0; stopNr < organ.organData.odm(manNr)->nrOfStops; stopNr++)
		{
			for (int rankNr = 0; rankNr < organ.organData.odm(manNr)->ods(stopNr)->nrOfRanks; rankNr++)
			{
				for (int pipeNr = 0; pipeNr < organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->highKeyNr - organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->lowKeyNr; pipeNr++)
				{
					if (organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->pipeData.channelnr == channel)
					{
						for (int bandNr = 0; bandNr < 31; bandNr++)
						{
							if (harmonicsEqualizer == 1)
							{
								sendStringToAdd = CStr(organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->pipeData.eqharmValue[bandNr]);
							}
							else
							{
								sendStringToAdd = CStr(organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->pipeData.eqoctValue[bandNr]);
							}

							if (sendStringToAdd.length() == 1) sendStringToAdd += "0";
							sendString += sendStringToAdd;

						}
						return "eqValues=" + sendString;

					}
				}
			}
		}
	}



}

string GetPipeDataVolume(Properties message)
{
	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = CInt(message["noteNr"]);
	int volNr = CInt(message["volNr"]);

	string sendString = "";
	string sendStringToAdd = "";

	if (volNr == 0)
	{
		sendString = "pipeData.Volume=" + CStr(((organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(noteNr)->pipeData.volume) * 100));
	}

	return sendString;
}

string GetPipeDataVolumeOnChannel(Properties message)
{

	int channelNr = CInt(message["channelNr"]);
	int volNr = CInt(message["volNr"]);
	string sendString = "";

	for (int manNr = 0; manNr < organ.organData.nrOfManuals; manNr++)
	{
		for (int stopNr = 0; stopNr < organ.organData.odm(manNr)->nrOfStops; stopNr++)
		{
			for (int rankNr = 0; rankNr < organ.organData.odm(manNr)->ods(stopNr)->nrOfRanks; rankNr++)
			{
				for (int pipeNr = 0; pipeNr < organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->highKeyNr - organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->lowKeyNr; pipeNr++)
				{
					if (organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->pipeData.channelnr == channelNr)
					{
						if (volNr == 0)
						{
							sendString = "pipeData.Volume=" + CStr(((organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->pipeData.volume) * 100));
						}
						return sendString;
					}
				}
			}
		}
	}



}

string GetSwellVolume(Properties message)
{
	int swellNr = CInt(message["swellNr"]);

	double valuedbl = 0.0;

	if (swellNr == 1)
	{
		valuedbl = organ.organData.volumeSwell1;
	}
	else if (swellNr == 2)
	{
		valuedbl = organ.organData.volumeSwell2;
	}
	return "value=" + CStr(valuedbl);

}

string loadSpec(Properties message)
{
	int specNr = CInt(message["specNr"]);

	bool wasUsingWindChests = soundMixer.useWindChests;
	if (wasUsingWindChests)
	{
		soundMixer.useWindChests = false;
		do
		{
			sleep(1);
		} while (soundMixer.windChestThreadLooping);
		soundMixer.useWindChests = true;
	}


	organ.loadOrgan(specNr);

	return"";

}

string PropertyFileLoad(Properties message) {

	Properties soc_PROPG = Properties(message["filename"]);
	string ret = "";

	for (props_iter_t i = soc_PROPG.begin(); i != soc_PROPG.end(); ++i) {

		ret = ret + (*i).first + "=" + (*i).second + "\n";

	}
	return ret;
}

string PropertyFileSave(Properties message) {
	string _fileName = message["filename"];
	message.DeleteProperty("filename");

	message.SaveProperties(_fileName);
	loadvdpmrdksysProperties(true);
	organ.loadOrganParameters(true);
	return "";
}

string SaveAllWavs(Properties message)
{
	for (int manNr = 0; manNr < organ.organData.nrOfManuals; manNr++)
	{
		for (int stopNr = 0; stopNr < organ.organData.odm(manNr)->nrOfStops; stopNr++)
		{
			for (int rankNr = 0; rankNr < organ.organData.odm(manNr)->ods(stopNr)->nrOfRanks; rankNr++)
			{
				try
				{
					if (organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->saveDataToFile(false))
					{
						organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->clearEquilizerData();
						organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->loadWavDataInMemory(useMemoryForSoundData);
					}
				}
				catch (...)
				{
					cout << "exception: " << manNr << " " << stopNr << " " << rankNr << endl;
				}
			}
		}
	}

}

string SetAllRegistersOn(Properties message)
{
	bool allOn = (message["SetAllRegistersOn"] == "true");

	if (allOn)
	{
		for (int manNr = 0; manNr < organ.organData.nrOfManuals; manNr++)
		{
			for (int stopNr = 0; stopNr < organ.organData.odm(manNr)->nrOfStops; stopNr++)
			{
				organ.openStop(manNr, stopNr);

			}
		}

	}
	else
	{
		for (int manNr = 0; manNr < organ.organData.nrOfManuals; manNr++)
		{
			for (int stopNr = 0; stopNr < organ.organData.odm(manNr)->nrOfStops; stopNr++)
			{
				organ.closeStop(manNr, stopNr);

			}
		}

	}

}

string SaveRank(Properties message)
{
	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int saveWav = CInt(message["saveWav"]);
	organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->saveDataToFile(saveWav);
	organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->clearEquilizerData();
	organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->loadWavDataInMemory(useMemoryForSoundData);
}

string SetEqSamplingSamples(Properties message)
{
	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = 0;
	int value = 0;

	vector<string> noteNrs = split(message["noteNrs"], '|');
	vector<string> values = split(message["values"], '|');

	for (int i = 0; i < noteNrs.size(); i++)
	{
		noteNr = CInt(noteNrs[i]);
		value = CInt(values[i]);
		organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(noteNr)->pipeData.eqSamplingSamples = value * 190;
	}

}

string SetEqSamplingSamplesFade(Properties message)
{
	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = 0;
	int value = 0;

	vector<string> noteNrs = split(message["noteNrs"], '|');
	vector<string> values = split(message["values"], '|');

	for (int i = 0; i < noteNrs.size(); i++)
	{
		noteNr = CInt(noteNrs[i]);
		value = CInt(values[i]);
		organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(noteNr)->pipeData.eqSamplingSamplesFade = value * 190;
	}
}

string SetPipeDataVolume(Properties message)
{

	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = 0; // CInt(message["noteNr"]);
	int volNr = CInt(message["volNr"]);  // volNr: 0 - volume  1 - windlevel

	int value = 0; // CInt(message["value"]);

	vector<string> noteNrs = split(message["noteNrs"], '|');
	vector<string> values = split(message["values"], '|');

	for (int i = 0; i < noteNrs.size(); i++)
	{
		noteNr = CInt(noteNrs[i]);
		value = CInt(values[i]);
		if (value < 0) value = value + 255; // signed to unsigned
		double volDouble = ((double)(value) / 100);
		organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(noteNr)->SetVolume(volNr, volDouble);
	}

}

string SetPipeDataVolumeForChannel(Properties message)
{
	int volNr = CInt(message["volNr"]);  // volNr: 0 - volume  1 - windlevel
	int value = CInt(message["value"]);
	int channel = CInt(message["channel"]);
	if (value < 0) value = value + 255; // signed to unsigned

	double volDouble = ((double)(value) / 100.0);

	for (int manNr = 0; manNr < organ.organData.nrOfManuals; manNr++)
	{
		for (int stopNr = 0; stopNr < organ.organData.odm(manNr)->nrOfStops; stopNr++)
		{
			for (int rankNr = 0; rankNr < organ.organData.odm(manNr)->ods(stopNr)->nrOfRanks; rankNr++)
			{
				for (int pipeNr = 0; pipeNr < organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->highKeyNr - organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->lowKeyNr; pipeNr++)
				{
					if (organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->pipeData.channelnr == channel)
					{
						organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->SetVolume(volNr, volDouble);

					}
				}
			}
		}
	}
}

string SetPitchRatioValue(Properties message)
{
	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = 0;
	int value = 0;

	vector<string> noteNrs = split(message["noteNrs"], '|');
	vector<string> values = split(message["values"], '|');

	for (int i = 0; i < noteNrs.size(); i++)
	{
		noteNr = CInt(noteNrs[i]);
		value = CInt(values[i]);
		organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(noteNr)->SetPitchRatioChange(value - 63);
	}
}

string SetSwellVolume(Properties message)
{

	int swellNr = CInt(message["swellNr"]);
	double value = CDbl(message["value"]);

	string sendString = "";
	string sendStringToAdd = "";

	if (swellNr == 1)
	{

		organ.organData.setVolumeSwell1(value);
		soundMixer.setSwell1ValueToPlayer(value);
	}
	else if (swellNr == 2)
	{
		organ.organData.setVolumeSwell2(value);
		soundMixer.setSwell2ValueToPlayer(value);
	}
}

string SetVoicingEqualizer(Properties message)
{

	/*
	* param 7: mannr
	* param 8: stopnr
	* param 9: ranknr
	* param 10: notenr
	* param 11: qtynotes
	* param 12: harmonics equalizer (0 or 1)
	* param 13: bandnr
	* param 14: value
	* param 15: preamp
	*/

	int manNr = CInt(message["manNr"]);
	int stopNr = CInt(message["stopNr"]);
	int rankNr = CInt(message["rankNr"]);
	int noteNr = 0; // CInt(message["noteNr"]);   weg, vanwege multinote
	int qtyNotes = CInt(message["qtyNotes"]);
	int harmonicsEqualizer = CInt(message["harmonicsEqualizer"]);
	int bandNr = CInt(message["bandNr"]);
	int value = CInt(message["value"]);
	int preamp = CInt(message["preamp"]);

	vector<string> noteNrs = split(message["noteNrs"], '|');

	for (int i = 0; i < noteNrs.size(); i++)
	{
		noteNr = CInt(noteNrs[i]);
		organ.organData.setEqualizer(manNr, stopNr, rankNr, noteNr, qtyNotes, harmonicsEqualizer == 1, bandNr, value, preamp);
	}
	//	organ.organData.setEqualizer(manNr, stopNr, rankNr, 0, qtyNotes, harmonicsEqualizer == 1, bandNr, value, preamp);
		//cout << noteNr << endl;


		//organ.organData.setEqualizer(int(msg[7]), int(msg[8]), int(msg[9]), int(msg[10]), int(msg[11]), (int(msg[12]) == 1), int(msg[13]), int(msg[14]) - 24, int(msg[15]) - 24);
}

string SetVoicingEqualizerForChannel(Properties message)
{

	/*param 12: harmonics equalizer(0 or 1)
		* param 13 : bandnr
		* param 14 : value
		* param 15 : preamp
		* param 16 : channel
		*/

	int harmonicsEqualizer = CInt(message["harmonicsEqualizer"]);
	int bandNr = CInt(message["bandNr"]);
	int value = CInt(message["value"]);;
	int preamp = CInt(message["preamp"]);
	int channel = CInt(message["channel"]);

	for (int manNr = 0; manNr < organ.organData.nrOfManuals; manNr++)
	{
		for (int stopNr = 0; stopNr < organ.organData.odm(manNr)->nrOfStops; stopNr++)
		{
			for (int rankNr = 0; rankNr < organ.organData.odm(manNr)->ods(stopNr)->nrOfRanks; rankNr++)
			{
				for (int pipeNr = 0; pipeNr < organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->highKeyNr - organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->lowKeyNr; pipeNr++)
				{
					if (organ.organData.odm(manNr)->ods(stopNr)->odr(rankNr)->odp(pipeNr)->pipeData.channelnr == channel)
					{
						organ.organData.setEqualizer(manNr, stopNr, rankNr, pipeNr, 1, harmonicsEqualizer == 1, bandNr, value, preamp);

					}
				}
			}
		}
	}

}



string parseSocketMessage(string msg) {

	Properties message;
	message.LoadPropertiesFromString(msg);
	string ret = "";
	string action = message["action"];
	message.erase("action");

	// cout << action << endl;

	if (action == "determine15pSamplePos") organ.determine15pSamplePos();

	if (action == "detune") DeTune(message);

	if (action == "ClearEquilizerData") organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->clearEquilizerData();

	if (action == "fillInStandardReverbChannelsAndVolumes") organ.fillInStandardReverbChannelsAndVolumes();

	if (action == "renameAllToVMS") organ.renameAllToVMS();

	if (action == "GetPipeData.Volume") ret = GetPipeDataVolume(message);

	if (action == "GetCouplerActive") ret = (organ.checkOpenedCouplerMidi(CInt(message["midiNr"])) ? "couplerActive=1" : "couplerActive=0");

	if (action == "GetEqValues") ret = GetEqValues(message);

	if (action == "GetEqValuesForChannel") ret = GetEqValuesForChannel(message);

	if (action == "GetSamplingSamples") ret = "value=" + CStr((organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->odp(CInt(message["noteNr"]))->pipeData.eqSamplingSamples / 190));

	if (action == "GetSamplingSamplesFade") ret = "value=" + CStr((organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->odp(CInt(message["noteNr"]))->pipeData.eqSamplingSamplesFade / 190));

	if (action == "GetPitchRatioChange") ret = "value=" + CStr((organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->odp(CInt(message["noteNr"]))->pipeData.pitchRatioChange + 63));

	if (action == "GetPipeDataVolumeOnChannel") ret = GetPipeDataVolumeOnChannel(message);

	if (action == "GetStopActive") ret = (organ.checkOpenedStopMidi(CInt(message["midiNr"])) ? "stopActive=1" : "stopActive=0");

	if (action == "GetSwellVolume") ret = GetSwellVolume(message);

	if (action == "GetThreadingOn") ret = (soundMixer.useThreads ? "GetThreadingOn=1" : "GetThreadingOn=0");


	if (action == "GetTremulantActive") ret = (organ.checkOpenedTremulantMidi(CInt(message["midiNr"])) ? "tremulantActive=1" : "tremulantActive=0");

	if (action == "loadSpec") loadSpec(message);

	if (action == "muteRank") organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->muteRank();

	if (action == "openCloseCouplerMidi") organ.openCloseCouplerMidi(CInt(message["midiNr"]));

	if (action == "openCloseStopMidi") organ.openCloseStopMidi(CInt(message["midiNr"]));

	if (action == "openCloseTremulantMidi") organ.openCloseTremulantMidi(CInt(message["midiNr"]));

	if (action == "playNote") organ.playNote(CInt(message["noteNr"]) - 36, CInt(message["manNr"]));

	if (action == "PropertyFileLoad") ret = PropertyFileLoad(message);

	if (action == "PropertyFileSave") ret = PropertyFileSave(message);

	if (action == "RankCalculateWavFreq") organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->calculateWAVFreq();

	if (action == "RenameRankVmsToWav") organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->renToWav();

	if (action == "saveDataToFile") organ.organData.saveDataToFile();

	if (action == "SaveAllWavs") SaveAllWavs(message);

	if (action == "SaveRank") SaveRank(message);

	if (action == "SetAllRegistersOn") SetAllRegistersOn(message);

	if (action == "SetEqualizer") useEqualizer = (message["equalizerOn"] == "true");

	if (action == "SetPipeDataVolume") SetPipeDataVolume(message);

	if (action == "SetPipeDataVolumeForChannel") SetPipeDataVolumeForChannel(message);

	if (action == "SetPitchRatioValue") SetPitchRatioValue(message);

	if (action == "SetVoicingEqualizer") SetVoicingEqualizer(message);

	if (action == "SetVoicingEqualizerForChannel") SetVoicingEqualizerForChannel(message);

	if (action == "SetEqSamplingSamples") ret = SetEqSamplingSamples(message);

	if (action == "SetEqSamplingSamplesFade") ret = SetEqSamplingSamplesFade(message);

	if (action == "SetSwellVolume") ret = SetSwellVolume(message);

	if (action == "SetVolume") organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->SetVolume(CDbl(message["value"]) / 50.0);

	if (action == "stopNote") organ.stopNote(CInt(message["noteNr"]) - 36, CInt(message["manNr"]));

	if (action == "stopOrgan") stopOrgan();

	if (action == "threadingOn") soundMixer.setThreading((message["threadingOn"] == "true"));

	if (action == "tuneOrgan") organ.tuneOrgan();

	if (action == "unMuteRank") organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->odr(CInt(message["rankNr"]))->unMuteRank();

	if (action == "renameStop") organ.organData.odm(CInt(message["manNr"]))->ods(CInt(message["stopNr"]))->renameStop(message["oldName"], message["newName"]);
	//cout << action << " " << ret << endl;
	//cout << action << " "  << CInt(message["manNr"]) << " " << CInt(message["stopNr"]) << " " << CInt(message["rankNr"]) << endl;


	return ret;
}


void errornew(const char *msg) {
	perror(msg);
	exit(1);
}

void startSocketThreadNew() {

	// determine stacksize for thread
	std::size_t stacksize;
	pthread_attr_init(&attrsocketThreadNew);
	pthread_attr_getstacksize(&attrsocketThreadNew, &stacksize);
	//cout << "Default stack size = " << stacksize << "\n";

	//init mutex for Queue
	pthread_mutex_init(&mutexQueueSocketThreadNew, nullptr);

	pthread_create(&socketThreadNew, nullptr, runSocketThreadNew, nullptr);
	msg("SocketThread Started");
}

void stopSocketThreadNew() {
	pthread_mutex_lock(&mutexQueueSocketThreadNew);
	gt_stopSocketThreadNew = true;
	pthread_mutex_unlock(&mutexQueueSocketThreadNew);

	dmsg("Joining SocketThread");

	pthread_join(socketThreadNew, nullptr);
	dmsg("Joined SocketThread");

	pthread_mutex_destroy(&mutexQueueSocketThreadNew);
	gt_SocketThreadNewStopped = true;
}

void *runSocketThreadNew(void *ptr) {
	socknewfd = socket(AF_INET, SOCK_STREAM, 0);
	if (socknewfd < 0)
		errornew("ERROR opening socket");

	bzero((char *)&serv_addrnew, sizeof(serv_addrnew));
	portnonew = 15557;
	serv_addrnew.sin_family = AF_INET;
	serv_addrnew.sin_addr.s_addr = INADDR_ANY;
	serv_addrnew.sin_port = htons(portnonew);

	if (bind(socknewfd, (struct sockaddr *) &serv_addrnew, sizeof(serv_addrnew)) < 0)
		errornew("ERROR on binding");

	while (!tickSocketThreadNew())
		;

	close(socknewfd);

	msg("SocketThread Stopped");
	pthread_exit(static_cast<void *>(nullptr));
}

bool tickSocketThreadNew() {
	int n;

	listen(socknewfd, 5);
	clilennew = sizeof(cli_addrnew);

	newsocknewfd = accept(socknewfd, reinterpret_cast<struct sockaddr *>(&cli_addrnew), &clilennew);
	if (newsocknewfd < 0) {
		errornew("ERROR on accept");
	}
	else {
		//cout << "socket connected!" << endl;
	}

	do {
		bzero(socketBuffernew, 256);
		n = read(newsocknewfd, socketBuffernew, 255);
		if (n < 0) errornew("ERROR reading from socket");

		if (n > 0) {

			const char *p_str;

			string ret = "";
			string data = "";
			string rcvString = "";


			while (true) {
				rcvString.assign(socketBuffernew);
				data += rcvString;
				if (data.find("<EOF>") != string::npos) {
					data = replace(data, "<EOF>", "");
					break;
				}
				bzero(socketBuffernew, 256);
				n = read(newsocknewfd, socketBuffernew, 255);
				if (n < 0) errornew("ERROR reading from socket");
			}
			ret = parseSocketMessage(data) + "<EOF>";

			p_str = ret.c_str();
			n = write(newsocknewfd, p_str, ret.length());
			if (n < 0) errornew("ERROR writing to socket");
		}
	} while (n > 0 && !gt_stopSocketThreadNew);

	close(newsocknewfd);
	//cout << "socket closed!" << endl;
	return gt_stopSocketThreadNew;
}
