#pragma once
/*
* File:   SocketThreadNew.h
* Author: marco
*
* Created on juni 8, 2017
*/

#include "global.h"

using namespace std;


#ifndef SOCKETTHREADNEW_H
#define	SOCKETTHREADNEW_H


// threading

void startSocketThreadNew();
void stopSocketThreadNew();
void *runSocketThreadNew(void *ptr);
bool tickSocketThreadNew();

extern bool gt_SocketThreadNewStopped;


#endif	/* MIXERTHREADNEW_H */