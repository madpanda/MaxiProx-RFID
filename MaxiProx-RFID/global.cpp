#include "global.h"
#include <iostream>
#include <string>
#include <sstream>

void msg(std::string smsg) {
	std::cout << smsg << "\n";
}

void dmsg(std::string smsg) {
#ifdef DBG
	msg(smsg);
#endif
}

std::string zeroPadding(int num, int totalNumbers) {
	std::stringstream ss;

	// the number is converted to string with the help of stringstream
	ss << num;
	std::string ret;
	ss >> ret;

	// Append zero chars
	int str_length = (int)ret.length();
	for (int i = 0; i < totalNumbers - str_length; i++)
		ret = "0" + ret;
	return ret;
}