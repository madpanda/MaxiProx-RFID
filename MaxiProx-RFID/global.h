// global header file

#include <string>

#include "SocketSendData.h"
#include "SocketServerOutThread.h"

void msg(std::string smsg);

void dmsg(std::string smsg);

std::string zeroPadding(int num, int totalNumbers);