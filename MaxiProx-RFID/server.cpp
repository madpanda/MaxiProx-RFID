/* The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "server.h"

int sockfd, newsockfd;
socklen_t clientlen;
char buffer[256];
struct sockaddr_in server_addr, cli_addr;
int n;

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

int initServer()
{
	
	// create a socket
	// socket(int domain, int type, int protocol)
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		error("ERROR opening socket");
		return 1;
	}

	// clear address structure
	bzero((char *) &server_addr, sizeof(server_addr));


	/* setup the host_addr structure for use in bind call */
	// server byte order
	server_addr.sin_family = AF_INET;

	// bind to any address
	server_addr.sin_addr.s_addr = INADDR_ANY;

	server_addr.sin_port = SERVERPORT;

	// bind(int fd, struct sockaddr *local_addr, socklen_t addr_length)
	// bind() passes file descriptor, the address structure, 
	// and the length of the address structure
	// This bind() call will bind  the socket to the current IP address on port, portno
	if (bind(sockfd, (struct sockaddr *) &server_addr,sizeof(server_addr)) < 0)
	{
		error("ERROR on binding");
		return 1;
	}
		
	// This listen() call tells the socket to listen to the incoming connections.
	// The listen() function places all incoming connection into a backlog queue
	// until accept() call accepts the connection.
	// Here, we set the maximum size for the backlog queue to 5.
	listen(sockfd, 5);

	// The accept() call actually accepts an incoming connection
	clientlen = sizeof(cli_addr);

	// This accept() function will write the connecting client's address info 
	// into the the address structure and the size of that structure is clilen.
	// The accept() returns a new socket file descriptor for the accepted connection.
	// So, the original socket file descriptor can continue to be used 
	// for accepting new connections while the new socker file descriptor is used for
	// communicating with the connected client.
	newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clientlen);
	if (newsockfd < 0)
		error("ERROR on accept");

	printf("server: got connection from %s port %d\n",
		inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));

	// This send() function sends the 13 bytes of the string to the new socket
	send(newsockfd, "Connected to socket\n, ", 19, 0);
	send(newsockfd, "Hello, world!\n", 13, 0);

	return 0;
}

void readMessage()
{
	bzero(buffer, 256);
	n = read(newsockfd, buffer, 255);
	if (n < 0) error("ERROR reading from socket");
	printf("Here is the message: %s\n", buffer);
}

void sendMessage(char* message, int messageLength)
{
	send(newsockfd, message, messageLength, 0);
}

int closeServer()
{
	close(newsockfd);
	close(sockfd);
	return 0;
}