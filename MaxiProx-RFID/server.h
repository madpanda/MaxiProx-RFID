#pragma once

#define SERVERPORT 8000

//#ifdef __cplusplus
//extern "C" {
//#endif
	int initServer(void);
	void readMessage(void);
	void sendMessage(char* message, int messageLength);
	int closeServer(void);


//#ifdef __cplusplus
//}
//#endif