## Wiegand to Node.js backend bridge written in C++

### Description in Dutch
C++ socket server die via twee GPIO pins van de Raspberry Pi de MaxiProx uitleest met de Wiegand interface.
De Node.js backend heeft een socket client die de (leesbare) data binnenhaalt.
